#include "StackV.h"

using namespace std;

int Stack::size()
{
	return data.size();
}

void Stack::push(int val)
{
	data.push_back(val);
}

void Stack::pop()
{
	data.pop_back();
}

int Stack::top()
{
	if (data.size() == 0)
	{
		return -1;
	}
	else
	{
	return data[(data.size() - 1)];
	}
}

void Stack::clear()
{
	data.clear();
}
